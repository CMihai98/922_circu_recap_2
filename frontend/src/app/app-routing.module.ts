import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MoviesListComponent} from './movies/movies-list/movies-list.component';
import {MovieDetailsComponent} from './movies/movie-details/movie-details.component';
import {OwnerListComponent} from './owners/owner-list/owner-list.component';
import {OwnerDetailsComponent} from './owners/owner-details/owner-details.component';


const routes: Routes = [
  {path: 'movieapp/movies', component: MoviesListComponent},
  {path: 'movieapp/movie/details/:id', component: MovieDetailsComponent},
  {path: 'animalcontest/contest-participants', component: OwnerListComponent},
  {path: 'animalcontest/contest-participants/details/:id', component: OwnerDetailsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
