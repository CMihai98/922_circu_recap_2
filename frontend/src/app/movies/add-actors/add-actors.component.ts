import {Component, Inject, OnInit} from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MovieService} from '../../shared/service/movie.service';
import {ActorService} from '../../shared/service/actor.service';
import {Actor} from '../../shared/model/actor.model';
import {Movie} from '../../shared/model/movie.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export class DialogData {
  movie: Movie;
}

@Component({
  selector: 'app-add-actors',
  templateUrl: './add-actors.component.html',
  styleUrls: ['./add-actors.component.scss']
})
export class AddActorsComponent implements OnInit {
  actors: Actor[];
  selectedActor: Actor = null;
  movie: Movie;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<AddActorsComponent>,
              private movieService: MovieService,
              private actorService: ActorService) { }

  ngOnInit(): void {
    this.getActors();
    this.movie = this.data.movie;
  }

  addActor() {
    console.log(this.selectedActor);
    this.movieService.updateMovie(this.movie.id, this.selectedActor.id)
      .subscribe(movie => {
        this.movie = movie;
        this.getActors();
      });
  }

  getActors(): void {
    this.actorService.getAvailableActors()
      .subscribe(actors => this.actors = actors);
  }

  close(): void {
    this.dialogRef.close(this.movie);
  }

}
