import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../shared/service/movie.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Movie} from '../../shared/model/movie.model';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AddActorsComponent} from '../add-actors/add-actors.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  movie: Movie;
  constructor(private movieService: MovieService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getActor();
  }

  getActor(): void {
    this.movieService.getMovieWithActors(
      +this.route.snapshot.paramMap.get('id'))
      .subscribe(movie => this.movie = movie);
  }

  back() {
    this.location.back();
  }

  openDialog() {
    this.dialog.open(AddActorsComponent,
      {width: '400px', data: {movie: this.movie}})
      .afterClosed().subscribe(movie => this.movie = movie);
  }
}
