import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../shared/service/movie.service';
import {Movie} from '../../shared/model/movie.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  movies: Movie[];

  constructor(private movieService: MovieService,
              private router: Router) { }

  ngOnInit(): void {
    this.movieService.getMovies()
      .subscribe(movies => this.movies = movies);
  }

  search(title: string): void {
    this.movieService.getMoviesByTitle(title)
      .subscribe(movies => this.movies = movies);
  }

  detail(movie: Movie): void {
    this.router
      .navigate(['movieapp/movie/details/', movie.id]);
  }
}
