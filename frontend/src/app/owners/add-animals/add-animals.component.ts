import {Component, Inject, OnInit} from '@angular/core';
import {Movie} from "../../shared/model/movie.model";
import {Owner} from "../../shared/model/owner.model";
import {Animal} from "../../shared/model/animal.model";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MovieService} from "../../shared/service/movie.service";
import {ActorService} from "../../shared/service/actor.service";
import {OwnerService} from "../../shared/service/owner.service";
import {AnimalService} from "../../shared/service/animal.service";


export class DialogData {
  owner: Owner;
}

class Animals {
}

@Component({
  selector: 'app-add-animals',
  templateUrl: './add-animals.component.html',
  styleUrls: ['./add-animals.component.scss']
})
export class AddAnimalsComponent implements OnInit {
  animals: Animal[];
  selectedAnimal: Animal = null;
  owner: Owner;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private dialogRef: MatDialogRef<AddAnimalsComponent>,
              private ownerService: OwnerService,
              private animalService: AnimalService) { }

  ngOnInit(): void {
    this.getAnimals();
    this.owner = this.data.owner;
  }

  addAnimal() {
    console.log(this.selectedAnimal);
    this.ownerService.update(this.owner.id, this.selectedAnimal.id)
      .subscribe(owner => {
        this.owner = owner;
        this.getAnimals();
      });
  }

  private getAnimals() {
    this.animalService.getAnimals()
      .subscribe(animals => this.animals = animals);
  }

  close(): void {
    this.dialogRef.close(this.owner);
  }
}
