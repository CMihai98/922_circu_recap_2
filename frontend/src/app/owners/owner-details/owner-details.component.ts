import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../shared/service/movie.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {OwnerService} from '../../shared/service/owner.service';
import {Owner} from '../../shared/model/owner.model';
import {Animal} from '../../shared/model/animal.model';
import {MatDialog} from '@angular/material/dialog';
import {AddAnimalsComponent} from '../add-animals/add-animals.component';

@Component({
  selector: 'app-owner-details',
  templateUrl: './owner-details.component.html',
  styleUrls: ['./owner-details.component.scss']
})
export class OwnerDetailsComponent implements OnInit {
  owner: Owner;
  breed = '';
  age: number;
  constructor(private ownerService: OwnerService,
              private route: ActivatedRoute,
              private location: Location,
              private dialog: MatDialog) { }

  ngOnInit(): void {
    this.getOwner();
  }

  private getOwner() {
    this.ownerService.getOwnerWithPets(
      +this.route.snapshot.paramMap.get('id'))
      .subscribe(owner => {
        this.owner = owner;
        console.log(owner);
      });
  }

  detail(pet: Animal) {
    this.breed = pet.breed;
    this.age = pet.age;
  }

  delete() {
    this.ownerService.delete(this.owner.id)
      .subscribe(_ => this.location.back());
  }

  openDialog() {
    this.dialog.open(AddAnimalsComponent,
      {width: '400px', data: {owner: this.owner}})
      .afterClosed().subscribe(owner => this.owner = owner);  }
}
