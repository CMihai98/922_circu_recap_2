import { Component, OnInit } from '@angular/core';
import {Owner} from '../../shared/model/owner.model';
import {OwnerService} from '../../shared/service/owner.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.scss']
})
export class OwnerListComponent implements OnInit {
  owners: Owner[];
  constructor(private ownerService: OwnerService,
              private router: Router) { }

  ngOnInit(): void {
    this.ownerService.getOwners()
      .subscribe(owners => this.owners = owners);
  }

  detail(id: number) {
    this.router.navigate(['animalcontest/contest-participants/details/', id]);
  }
}
