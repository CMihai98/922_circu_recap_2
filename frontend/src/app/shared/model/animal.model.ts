import {animalTypes} from './animalTypes.enum';
import {Owner} from './owner.model';

export class Animal {
  id: number;
  name: string;
  breed: string;
  age: number;
  type: animalTypes;
  points: number;
  owner: Owner;
}
