import {Animal} from './animal.model';
import {Movie} from "./movie.model";
import {Observable} from "rxjs";

export class Owner {
  id: number;
  name: string;
  email: string;
  pets: Animal[];
}
