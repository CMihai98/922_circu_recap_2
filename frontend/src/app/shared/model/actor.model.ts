import {Movie} from './movie.model';

export class Actor {
  id: number;
  name: string;
  rating: number;
  movie: Movie;
}
