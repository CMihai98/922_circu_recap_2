import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Actor} from '../model/actor.model';

@Injectable({
  providedIn: 'root'
})
export class ActorService {
  actorsURL = 'http://localhost:8080/api/actors';

  constructor(private httpClient: HttpClient) {
  }

  getActors(): Observable<Actor[]> {
    return this.httpClient
      .get<Actor[]>(this.actorsURL);
  }

  getAvailableActors(): Observable<Actor[]> {
    const getURL = `${this.actorsURL}Available`;
    return this.httpClient
      .get<Actor[]>(getURL);
  }
}
