import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from '../model/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  moviesURL = 'http://localhost:8080/api/movies';

  constructor(private httpClient: HttpClient) {
  }

  getMovies(): Observable<Movie[]> {
    return this.httpClient
      .get<Movie[]>(this.moviesURL);
  }

  getMoviesByTitle(title: string): Observable<Movie[]> {
    const getURL = `${this.moviesURL}/${title}`;
    return this.httpClient
      .get<Movie[]>(getURL);
  }

  getMovieWithActors(movieId: number): Observable<Movie> {
    const getURL = `${this.moviesURL}/detail/${movieId}`;
    return this.httpClient
      .get<Movie>(getURL);
  }

  updateMovie(movieId: number, actorId: number): Observable<Movie> {
    const updateURL = `${this.moviesURL}/${movieId}/${actorId}`;
    return this.httpClient
      .get<Movie>(updateURL);
  }
}
