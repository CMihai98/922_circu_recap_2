import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Owner} from '../model/owner.model';
import {Animal} from '../model/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  animalsURL = 'http://localhost:8080/api/animals';

  constructor(private httpClient: HttpClient) {
  }

  getAnimals(): Observable<Animal[]> {
    return this.httpClient
      .get<Animal[]>(this.animalsURL);
  }
}
