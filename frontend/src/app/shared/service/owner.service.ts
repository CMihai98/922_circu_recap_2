import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Owner} from '../model/owner.model';
import {Movie} from '../model/movie.model';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {
  ownersURL = 'http://localhost:8080/api/owners';

  constructor(private httpClient: HttpClient) {
  }

  getOwners(): Observable<Owner[]> {
    return this.httpClient
      .get<Owner[]>(this.ownersURL);
  }

  getOwnerWithPets(id: number): Observable<Owner> {
    const getURL = `${this.ownersURL}/${id}`;
    return this.httpClient
      .get<Owner>(getURL);
  }

  delete(id: number) {
    const deleteURL = `${this.ownersURL}/${id}`;
    return this.httpClient
      .delete(deleteURL);
  }

  update(ownerId: number, animalId: number): Observable<Owner> {
    const updateURL = `${this.ownersURL}/${ownerId}/${animalId}`;
    return this.httpClient
      .get<Owner>(updateURL);
  }
}
