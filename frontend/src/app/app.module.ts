import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import {HttpClientModule} from '@angular/common/http';
import {MovieService} from './shared/service/movie.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';
import { AddActorsComponent } from './movies/add-actors/add-actors.component';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {OwnerService} from './shared/service/owner.service';
import { OwnerListComponent } from './owners/owner-list/owner-list.component';
import { OwnerDetailsComponent } from './owners/owner-details/owner-details.component';
import { AddAnimalsComponent } from './owners/add-animals/add-animals.component';
import {AnimalService} from "./shared/service/animal.service";

@NgModule({
  declarations: [
    AppComponent,
    MoviesListComponent,
    MovieDetailsComponent,
    AddActorsComponent,
    OwnerListComponent,
    OwnerDetailsComponent,
    AddAnimalsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatDialogModule
  ],
  providers: [MovieService, OwnerService, AnimalService, {
    provide: MatDialogRef,
    useValue: {}
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
