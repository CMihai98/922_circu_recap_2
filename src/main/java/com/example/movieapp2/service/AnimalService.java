package com.example.movieapp2.service;

import com.example.movieapp2.model.Animal;
import com.example.movieapp2.repository.AnimalRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnimalService {
    private final AnimalRepository animalRepository;

    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    public List<Animal> getAllAnimals(){
        return animalRepository.findAnimalsByOwnerIsNull();
    }

    public List<Animal> getAnimalsByTypeAgeAndName(String type, int age, String name){
        return animalRepository.findAnimalsByTypeAndAgeAndName(type, age, name);
    }

//    @Transactional
//    public Animal save(Animal animal){
//        return animalRepository.save(animal);
//    }

    @Transactional
    public void deleteAnimal(Long animalId){
        animalRepository.findById(animalId).ifPresent(animal -> {
            animalRepository.delete(animal);
        });

    }


}
