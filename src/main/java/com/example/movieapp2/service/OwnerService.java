package com.example.movieapp2.service;

import com.example.movieapp2.model.Animal;
import com.example.movieapp2.model.Owner;
import com.example.movieapp2.repository.AnimalRepository;
import com.example.movieapp2.repository.OwnerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OwnerService {

    public final OwnerRepository ownerRepository;
    public final AnimalRepository animalRepository;

    public OwnerService(OwnerRepository movieRepository, AnimalService animalService, OwnerRepository ownerRepository, AnimalRepository animalRepository) {
        this.ownerRepository = ownerRepository;
        this.animalRepository = animalRepository;
    }

    public List<Owner> getOwners() {
        return this.ownerRepository.findAll();
    }


    public Owner getOwnerWithPets(Long id) {
        return this.ownerRepository.findOwnerById(id);
    }

//    @Transactional
//    public Owner save(Owner owner) {
//        return ownerRepository.save(owner);
//    }

    @Transactional
    public Owner addAnimal(Long ownerId, Long petId) {
        Owner owner = ownerRepository.findOwnerById(ownerId);
        Animal pet = animalRepository.findById(petId).orElseThrow();
        owner.getPets().add(pet);
        pet.setOwner(owner);

        return owner;
    }
    @Transactional
    public void delete(Long id) {
        Owner owner = ownerRepository.findOwnerById(id);
        ownerRepository.delete(owner);
    }
}
