package com.example.movieapp2.controller;

import com.example.movieapp2.mappers.AnimalMapper;
import com.example.movieapp2.model.dto.AnimalDTO;
import com.example.movieapp2.service.AnimalService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class AnimalController {

    private final AnimalService animalService;
    private final AnimalMapper animalMapper;

    public AnimalController(AnimalService animalService, AnimalMapper animalMapper) {
        this.animalService = animalService;
        this.animalMapper = animalMapper;
    }
//
//    @RequestMapping(value = "/animals", method = RequestMethod.POST)
//    public AnimalDTO save(@RequestBody AnimalDTO animalDTO){
//        return animalMapper.toDto(
//                animalService.save(
//                        animalMapper.fromDto(animalDTO)));
//    }

    @RequestMapping(value = "/animals", method = RequestMethod.GET)
    public List<AnimalDTO> getAllAnimals(){
        return animalMapper.toDtoList(
                animalService.getAllAnimals());
    }

}
