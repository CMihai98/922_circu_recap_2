package com.example.movieapp2.controller;

import com.example.movieapp2.mappers.OwnerMapper;
import com.example.movieapp2.model.dto.OwnerDTO;
import com.example.movieapp2.service.OwnerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class OwnerController {

    public final OwnerService ownerService;
    public final OwnerMapper ownerMapper;

    public OwnerController(OwnerService ownerService, OwnerMapper ownerMapper) {
        this.ownerService = ownerService;
        this.ownerMapper = ownerMapper;
    }

    @RequestMapping(value = "/owners", method = RequestMethod.GET)
    public List<OwnerDTO> getOwners(){
        return ownerMapper.toDtoList(
                ownerService.getOwners());
    }
//
//    @RequestMapping(value = "/movies/{title}", method = RequestMethod.GET)
//    public List<MovieDTO> getMovies(@PathVariable String title){
//        return movieMapper.toDtoList(
//                movieService.getMoviesByTitle(title));
//    }
//
//    @RequestMapping(value = "/owners", method = RequestMethod.POST)
//    public OwnerDTO save(@RequestBody OwnerDTO ownerDTO){
//        return ownerMapper.toDto(
//                ownerService.save(
//                        ownerMapper.fromDto(ownerDTO)));
//    }

    @RequestMapping(value = "/owners/{ownerId}/{animalId}", method = RequestMethod.GET)
    public OwnerDTO update(@PathVariable Long ownerId, @PathVariable Long animalId){
        return ownerMapper.toDtoWithPets(
                ownerService.addAnimal(ownerId, animalId));
    }

    @RequestMapping(value = "/owners/{id}", method = RequestMethod.GET)
    public OwnerDTO getOwnerWithPets(@PathVariable Long id){
        return ownerMapper.toDtoWithPets(
                ownerService.getOwnerWithPets(id));
    }

    @RequestMapping(value = "/owners/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteActor(@PathVariable Long id){
        ownerService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
