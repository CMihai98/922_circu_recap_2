package com.example.movieapp2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Data
@Entity
public class Animal extends BaseEntity {
    private String name;
    private String breed;

    private int age;

    private animalTypes type;

    private int points;

    @ManyToOne(fetch = FetchType.LAZY)
    private Owner owner;
}
