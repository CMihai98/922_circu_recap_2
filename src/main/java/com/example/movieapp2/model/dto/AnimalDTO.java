package com.example.movieapp2.model.dto;

import com.example.movieapp2.model.Owner;
import com.example.movieapp2.model.animalTypes;
import lombok.*;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AnimalDTO {
    private Long id;
    private String name;
    private String breed;
    private int age;
    private animalTypes type;
    private int points;
    private Owner owner;
}
