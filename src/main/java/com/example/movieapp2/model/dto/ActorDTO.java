package com.example.movieapp2.model.dto;

import com.example.movieapp2.model.Animal;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ActorDTO {
    private Long id;
    private String name;
    private int rating;
    private Animal movie;
}
