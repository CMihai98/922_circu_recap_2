package com.example.movieapp2.model.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class MovieDTO {
    private Long id;
    private String title;
    private int year;
    private List<ActorDTO> actors;
}
