package com.example.movieapp2.model.dto;

import com.example.movieapp2.model.Animal;
import lombok.*;

import javax.persistence.Column;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class OwnerDTO {
    private Long id;
    private String name;
    private String email;
    private List<AnimalDTO> pets;
}
