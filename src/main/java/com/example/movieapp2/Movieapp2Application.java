package com.example.movieapp2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Movieapp2Application {

    public static void main(String[] args) {
        SpringApplication.run(Movieapp2Application.class, args);
    }

}
