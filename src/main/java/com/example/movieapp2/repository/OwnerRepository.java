package com.example.movieapp2.repository;

import com.example.movieapp2.model.Owner;
import org.springframework.data.jpa.repository.EntityGraph;

public interface OwnerRepository extends AnimalContestRepository<Owner> {

    @EntityGraph(attributePaths = {"pets"})
    Owner findOwnerById(Long ownerId);
}
