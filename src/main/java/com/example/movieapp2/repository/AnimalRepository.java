package com.example.movieapp2.repository;

import com.example.movieapp2.model.Animal;

import java.util.List;

public interface AnimalRepository extends AnimalContestRepository<Animal> {

    List<Animal> findAnimalsByOwnerIsNull();

    List<Animal> findAnimalsByTypeAndAgeAndName(String type, int age, String name);

}
