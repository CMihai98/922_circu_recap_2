package com.example.movieapp2.repository;

import com.example.movieapp2.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalContestRepository<T extends BaseEntity> extends JpaRepository<T, Long> {
}
