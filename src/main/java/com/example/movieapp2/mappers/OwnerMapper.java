package com.example.movieapp2.mappers;

import com.example.movieapp2.model.Owner;
import com.example.movieapp2.model.dto.MovieDTO;
import com.example.movieapp2.model.dto.OwnerDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper(uses = {AnimalMapper.class})
public interface OwnerMapper {

    @Mapping(target = "pets", ignore = true)
    OwnerDTO toDto(Owner movie);

    @Named(value = "toDtoWithPets")
    @IterableMapping(qualifiedByName = "withoutOwner")
    OwnerDTO toDtoWithPets(Owner owner);

    @InheritInverseConfiguration(name = "toDtoWithPets")
    Owner fromDto(OwnerDTO ownerDTO);

    List<OwnerDTO> toDtoList(List<Owner> owners);

    @IterableMapping(qualifiedByName = "toDtoWithPets")
    List<OwnerDTO> toDtoListWithPets(List<Owner> movies);
}
