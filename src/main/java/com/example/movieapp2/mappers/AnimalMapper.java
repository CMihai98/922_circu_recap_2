package com.example.movieapp2.mappers;

import com.example.movieapp2.model.Animal;
import com.example.movieapp2.model.Owner;
import com.example.movieapp2.model.dto.ActorDTO;
import com.example.movieapp2.model.dto.AnimalDTO;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface AnimalMapper {

    @Mapping(target = "owner", ignore = true)
    @Named(value = "withoutOwner")
    AnimalDTO toDto(Animal animal);

    @Mapping(target = "owner", ignore = true)
    @Named(value = "withoutOwnerFrom")
    Animal fromDto(AnimalDTO animalDTO);

    @IterableMapping(qualifiedByName = "withoutOwner")
    List<AnimalDTO> toDtoList(List<Animal> animals);

    @IterableMapping(qualifiedByName = "withoutOwnerFrom")
    List<Animal> fromDtoList(List<AnimalDTO> actorDTOS);
}
